#!/bin/sh

if type wget >/dev/null 2>&1; then
  COM=wget
  OPT_R=
  OPT_H=--header=
  OPT_O=-O
elif type curl >/dev/null 2>&1; then
  COM='curl -L'
  OPT_R=-O
  OPT_H='-H '
  OPT_O=-o
else
  exit 0
fi

set $(uname -sm)
KERNEL=$1
ARCH=$2
if [ $KERNEL = Linux ]; then
  ECLIPSE_KERNEL=linux-gtk
  INI_DIR=/eclipse
  SED=
elif [ $KERNEL = Darwin ]; then
  ECLIPSE_KERNEL=macosx-cocoa
  INI_DIR=/Eclipse.app/Contents/Eclipse
  SED=\'\'
else
  exit 0
fi
if [ $ARCH = x86_64 ]; then
  JAVA_ARCH=x64
  ECLIPSE_ARCH=-x86_64
elif [ $ARCH = i686 ] || [ $ARCH = i586 ]; then
  JAVA_ARCH=i586
  ECLIPSE_ARCH=
elif [ $KERNEL = Darwin ] && [ $ARCH = i386 ]; then
  ECLIPSE_ARCH=
else
  exit 0
fi

if [ $KERNEL = Linux ]; then
  $COM $OPT_R $OPT_H'Cookie: oraclelicense=accept-securebackup-cookie' http://download.oracle.com/otn-pub/java/jdk/8u73-b02/jdk-8u73-linux-$JAVA_ARCH.tar.gz
fi
eval $COM $OPT_R http://ftp.yz.yamagata-u.ac.jp/pub/eclipse//eclipse/downloads/drops4/R-4.5.2-201602121500/eclipse-platform-4.5.2-$ECLIPSE_KERNEL$ECLIPSE_ARCH.tar.gz
eval "$COM $OPT_O pleiades.zip 'http://sourceforge.jp/projects/mergedoc/svn/view/trunk/Pleiades/build/pleiades.zip?view=co&root=mergedoc'"
tar xzvf eclipse-platform-4.5.2-$ECLIPSE_KERNEL$ECLIPSE_ARCH.tar.gz -C ~
if [ $KERNEL = Linux ]; then
  tar xzvf jdk-8u73-linux-$JAVA_ARCH.tar.gz -C ~/eclipse
  mv ~/eclipse/jdk1.8.0_73 ~/eclipse/jre
fi
unzip -d $HOME$INI_DIR/dropins/eclipse pleiades.zip plugins/\* features/\*
eval sed -i $SED '/-showsplash/d' $HOME$INI_DIR/eclipse.ini
eval sed -i $SED 's/MaxPermSize/MaxMetaspaceSize/g' $HOME$INI_DIR/eclipse.ini
eval sed -i $SED '/org.eclipse.platform/d' $HOME$INI_DIR/eclipse.ini
echo -Xverify:none >> $HOME$INI_DIR/eclipse.ini
echo -javaagent:$HOME$INI_DIR/dropins/eclipse/plugins/jp.sourceforge.mergedoc.pleiades/pleiades.jar >> $HOME$INI_DIR/eclipse.ini
