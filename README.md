# 最小Eclipseセットアップヘルパー

EclipseのPlatform Runtime BinaryにPleiadesを入れただけの環境を簡単に構築します。

## 使い方

    $ wget -O- https://bitbucket.org/nobby451/setup-minimal-eclipse/raw/master/setup-minimal-eclipse.sh | $SHELL

or

    $ curl https://bitbucket.org/nobby451/setup-minimal-eclipse/raw/master/setup-minimal-eclipse.sh | $SHELL

HOMEディレクトリーの下にeclipseという名前で展開されます。

pleiadesはdropins/eclipseの直下に展開されます。

Linuxの場合はJDKも一緒に展開されます。

## 動作環境

LinuxとMacのdash,bash,zshで大体動くように作っているつもりです。

動作確認はコミットした時の最新のLubuntuで行っています。
